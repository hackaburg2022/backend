package repository

import "gitlab.com/hackaburg2022/backend/internal/models"

type Ingredient interface {
	GetIngredientByID(id string) (*models.Ingredient, error)
	GetIngredientLikeByName(name string, size int, skip int) ([]*models.Ingredient, error)
}
