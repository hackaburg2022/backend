package repository

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"

	"github.com/mitchellh/mapstructure"

	"gitlab.com/hackaburg2022/backend/internal/models"
)

func (r *repository) GetIngredientByID(id string) (*models.Ingredient, error) {
	return nil, nil
}

func (r *repository) GetIngredientLikeByName(name string, size int, skip int) ([]*models.Ingredient, error) {
	if size == 0 {
		size = 20
	}
	db := r.db
	var result map[string]interface{}
	var req bytes.Buffer
	query := map[string]interface{}{
		"size": size,
		"from": skip,
		"query": map[string]interface{}{
			"prefix": map[string]interface{}{
				"name.keyword": map[string]interface{}{
					"value":            name,
					"case_insensitive": true,
				},
			},
		},
		"collapse": map[string]string{
			"field": "name.keyword",
		},
	}
	if err := json.NewEncoder(&req).Encode(query); err != nil {
		return nil, err
	}

	res, err := db.Search(
		db.Search.WithContext(context.Background()),
		db.Search.WithIndex("ingredients"),
		db.Search.WithBody(&req),
		db.Search.WithTrackTotalHits(true),
		db.Search.WithPretty(),
	)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.IsError() {
		return nil, fmt.Errorf("error: %s", res.String())
	}

	if err := json.NewDecoder(res.Body).Decode(&result); err != nil {
		return nil, err
	}

	// data := result["hits"].(map[string]interface{})["data"].(map[string]interface{})["data"]
	hits := result["hits"].(map[string]interface{})["hits"].([]interface{})
	ingredients := []*models.Ingredient{}

	for _, hit := range hits {
		raw := hit.(map[string]interface{})["_source"].(map[string]interface{})
		ingredient := models.Ingredient{}
		if err := mapstructure.Decode(raw, &ingredient); err != nil {
			return nil, err
		}
		ingredients = append(ingredients, &ingredient)
	}

	return ingredients, nil
}
