package repository

import (
	"github.com/elastic/go-elasticsearch/v8"
)

type repository struct {
	db *elasticsearch.Client
}

func NewRepository(db *elasticsearch.Client) (Repository, error) {
	return &repository{
		db: db,
	}, nil
}
