package repository

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/hackaburg2022/backend/internal/models"
)

func (r *repository) GetRecipesByLikedAndNotLikedIngredients(liked []string, notLiked []string, size int, skip int) ([]*models.Recipe, error) {
	if size == 0 {
		size = 20
	}
	db := r.db
	var result map[string]interface{}
	var req bytes.Buffer

	// var likes []interface{}
	likes := make([]interface{}, 0)
	for _, entry := range liked {
		should := []interface{}{
			map[string]interface{}{
				"match": map[string]interface{}{
					"ingredients.name": entry,
				},
			},
		}
		like := map[string]interface{}{
			"bool": map[string]interface{}{
				"should": should,
			},
		}
		likes = append(likes, like)
	}

	notLikes := make([]interface{}, 0)
	for _, entry := range notLiked {
		should := []interface{}{
			map[string]interface{}{
				"match": map[string]interface{}{
					"ingredients.name": entry,
				},
			},
		}
		notLike := map[string]interface{}{
			"bool": map[string]interface{}{
				"must_not": map[string]interface{}{
					"bool": map[string]interface{}{
						"should": should,
					},
				},
			},
		}
		notLikes = append(notLikes, notLike)
	}

	query := map[string]interface{}{
		"from": skip,
		"size": size,
		"query": map[string]interface{}{
			"bool": map[string]interface{}{
				"must": append(likes, notLikes...),
			},
		},
	}

	if err := json.NewEncoder(&req).Encode(query); err != nil {
		return nil, err
	}

	res, err := db.Search(
		db.Search.WithContext(context.Background()),
		db.Search.WithIndex("recipes"),
		db.Search.WithBody(&req),
		db.Search.WithTrackTotalHits(true),
		db.Search.WithPretty(),
	)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if res.IsError() {
		return nil, fmt.Errorf("error: %s", res.String())
	}

	if err := json.NewDecoder(res.Body).Decode(&result); err != nil {
		return nil, err
	}

	hits := result["hits"].(map[string]interface{})["hits"].([]interface{})
	recipes := []*models.Recipe{}

	for _, hit := range hits {
		raw := hit.(map[string]interface{})["_source"].(map[string]interface{})
		recipe := models.Recipe{}
		if err := mapstructure.Decode(raw, &recipe); err != nil {
			return nil, err
		}
		recipes = append(recipes, &recipe)
	}

	return recipes, nil
}

func (repo *repository) GetRecipesByIngredients(ingredients []string) ([]*models.Recipe, error) {
	return nil, nil
}

func (repo *repository) GetRecipeByID(id string) (*models.Recipe, error) {
	return nil, nil
}
