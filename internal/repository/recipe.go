package repository

import "gitlab.com/hackaburg2022/backend/internal/models"

type Recipe interface {
	GetRecipeByID(id string) (*models.Recipe, error)
	GetRecipesByIngredients(ingredients []string) ([]*models.Recipe, error)
	GetRecipesByLikedAndNotLikedIngredients(liked []string, notLiked []string, size int, skip int) ([]*models.Recipe, error)
}
