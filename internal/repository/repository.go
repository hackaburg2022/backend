package repository

type Repository interface {
	Recipe
	Ingredient
}
