package models

type RecipeRating struct {
	Rating   float32 `json:"rating"`
	NumVotes int     `json:"numVotes"`
}

type Recipe struct {
	ID                      string              `json:"id"`
	Title                   string              `json:"title"`
	SiteUrl                 string              `json:"siteUrl"`
	PreviewImageUrlTemplate string              `json:"previewImageUrlTemplate"`
	Ingredients             []map[string]string `json:"ingredients"`
	Tags                    []string            `json:"tags"`
	Rating                  RecipeRating        `json:"rating"`
}
