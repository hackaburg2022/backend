package models

type Ingredient struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
