//go:build wireinject
// +build wireinject

package router

import (
	"github.com/google/wire"
	"gitlab.com/hackaburg2022/backend/cmd/config"
	"gitlab.com/hackaburg2022/backend/internal/repository"
	v1 "gitlab.com/hackaburg2022/backend/internal/router/v1"
	"go.uber.org/zap"
)

func newRouter(config *config.Config, logger *zap.Logger, repo repository.Repository) *router {
	wire.Build(
		newEcho,
		// services.ProviderSet,
		wire.Struct(new(v1.Router), "*"),
		wire.Struct(new(router), "*"),
	)
	return nil
}
