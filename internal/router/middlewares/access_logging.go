package middlewares

import (
	"encoding/json"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/hackaburg2022/backend/internal/router/consts"
	"gitlab.com/hackaburg2022/backend/internal/router/extension"
	"go.uber.org/zap"
)

func AccessLoggingIgnore() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Set(consts.KeyIgnoreLogging, true)
			return next(c)
		}
	}
}

func AccessLogging(logger *zap.Logger, dev bool) echo.MiddlewareFunc {
	if dev {
		return func(next echo.HandlerFunc) echo.HandlerFunc {
			return func(c echo.Context) error {
				c.Set(consts.KeyLogger, logger)
				start := time.Now()
				if err := next(c); err != nil {
					c.Error(err)
				}
				stop := time.Now()

				ignoreLogging := c.Get(consts.KeyIgnoreLogging)
				if ignoreLogging != nil {
					if ignoreLogging.(bool) {
						return nil
					}
				}

				req := c.Request()
				res := c.Response()
				logger.Sugar().Infof("%3d | %s | %s %s %d", res.Status, stop.Sub(start), req.Method, req.URL, res.Size)
				return nil
			}
		}
	}
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Set(consts.KeyLogger, logger)
			start := time.Now()
			if err := next(c); err != nil {
				c.Error(err)
			}
			stop := time.Now()

			ignoreLogging := c.Get(consts.KeyIgnoreLogging)
			if ignoreLogging != nil {
				if ignoreLogging.(bool) {
					return nil
				}
			}

			req := c.Request()
			res := c.Response()
			type HTTPRequest struct {
				Status        int    `json:"status"`
				RequestMethod string `json:"requestMethod"`
				RequestURL    string `json:"requestUrl"`
				RequestSize   string `json:"requestSize"`
				ResponseSize  int64  `json:"responseSize"`
				UserAgent     string `json:"userAgent"`
				RemoteIP      string `json:"remoteIp"`
				ServerIP      string `json:"serverIp"`
				Referer       string `json:"referer"`
				Latency       string `json:"latency"`
				Protocol      string `json:"protocol"`
			}
			msg := struct {
				RequestID   string      `json:"requestId"`
				HTTPRequest HTTPRequest `json:"httpRequest"`
			}{
				RequestID: extension.GetRequestID(c),
				HTTPRequest: HTTPRequest{
					RequestMethod: req.Method,
					RequestURL:    req.URL.String(),
					RequestSize:   req.Header.Get(echo.HeaderContentLength),
					Status:        res.Status,
					ResponseSize:  res.Size,
					UserAgent:     req.UserAgent(),
					RemoteIP:      c.RealIP(),
					ServerIP:      c.Echo().Server.Addr,
					Referer:       req.Referer(),
					Latency:       strconv.FormatFloat(stop.Sub(start).Seconds(), 'f', 9, 64) + "s", // nolint:gomnd
					Protocol:      req.Proto,
				},
			}
			msgJSON, _ := json.Marshal(&msg)
			logger.Info(string(msgJSON))
			return nil
		}
	}
}
