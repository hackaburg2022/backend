package consts

const (
	KeyIgnoreLogging = "ignoreLogging"
	KeyIgnoreMetrics = "ignoreMetrics"
	KeyConfig        = "_config"
	KeyRepository    = "_repository"
	KeyLogger        = "_logger"
	KeyToken         = "_token"
	KeyUser          = "_user"
	KeyStation       = "_station"
)
