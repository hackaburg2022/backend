package router

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/hackaburg2022/backend/cmd/config"
	"gitlab.com/hackaburg2022/backend/internal/repository"
	"gitlab.com/hackaburg2022/backend/internal/router/extension"
	"gitlab.com/hackaburg2022/backend/internal/router/middlewares"
	v1 "gitlab.com/hackaburg2022/backend/internal/router/v1"
	"go.uber.org/zap"
)

type router struct {
	e  *echo.Echo
	v1 *v1.Router
}

func New(cfg *config.Config, logger *zap.Logger, repository repository.Repository) (*echo.Echo, error) {
	r := newRouter(cfg, logger, repository)
	r.v1.Setup(r.e.Group("/v1"))

	r.e.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, "")
	})

	if cfg.DevMode {
		routes := r.e.Routes()
		logger.Debug("handling following routes")
		for _, route := range routes {
			logger.Sugar().Debugf("\tpath: %s, method: %s", route.Path, route.Method)
		}
	}

	return r.e, nil
}

func newEcho(cfg *config.Config, repo repository.Repository, logger *zap.Logger) *echo.Echo {
	const maxAge = 300
	e := echo.New()

	e.HideBanner = true
	e.HidePort = true
	e.HTTPErrorHandler = extension.ErrorHandler(logger)

	e.Use(middlewares.RequestID())
	e.Use(extension.Wrap(repo, cfg))
	e.Use(middlewares.AccessLogging(logger.Named("access_log"), cfg.DevMode))
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:  []string{"*"},
		ExposeHeaders: []string{echo.HeaderXRequestID},
		AllowHeaders:  []string{"x-api-key", echo.HeaderContentType, echo.HeaderAuthorization},
		MaxAge:        maxAge,
	}))
	return e
}
