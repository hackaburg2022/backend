package v1

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func (r *Router) GetAllergy(c echo.Context) error {
	name := c.QueryParam("data")
	if name == "" {
		return echo.NewHTTPError(http.StatusBadRequest, "query not provided")
	}

	return c.JSON(http.StatusOK, mockAllergy())
}
