package v1

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/hackaburg2022/backend/cmd/config"
	"gitlab.com/hackaburg2022/backend/internal/repository"
	"go.uber.org/zap"
)

type Router struct {
	Logger     *zap.Logger
	Config     *config.Config
	Repository repository.Repository
}

func (r *Router) Setup(e *echo.Group) {
	e.GET("/ingredient", r.GetIngredients)
	e.GET("/allergy", r.GetAllergy)
	e.GET("/recipe", r.GetRecipe)
}
