package v1

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

type reqIngredients struct {
	Size int    `query:"size"`
	Skip int    `query:"skip"`
	Data string `query:"data"`
}

func (r *Router) GetIngredients(c echo.Context) error {
	req := reqIngredients{}

	if err := echo.QueryParamsBinder(c).
		Int("size", &req.Size).
		Int("skip", &req.Skip).
		String("data", &req.Data).
		BindError(); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	result, err := r.Repository.GetIngredientLikeByName(req.Data, req.Size, req.Skip)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	return c.JSON(http.StatusOK, result)
}
