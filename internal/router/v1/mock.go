package v1

func mockAllergy() []ResponseAllergy {
	return []ResponseAllergy{{
		ID:   "1234",
		Name: "Wurstallergie",
	}, {
		ID:   "1337",
		Name: "Tomatenallergie",
	}}
}

func mockIngerient() []ResponseIngredient {
	return []ResponseIngredient{{
		ID:   "1234",
		Name: "Tomate",
	}, {
		ID:   "2345",
		Name: "Wurst",
	}, {
		ID:   "3214",
		Name: "Gurke",
	}}
}

func mockRecipe() []ResponseRecipe {
	return []ResponseRecipe{{
		ID:          "1234",
		Name:        "Wursauflauf mit Wurst",
		ImgURL:      "https://ais.kochbar.de/kbrezept/435785_554123/24-05xcp0/1200x900/2170/nuernberger-eintopf-rezept.jpg",
		Ingredients: []string{"Wurst", "Käse"},
	}, {
		ID:          "2341",
		Name:        "Nudeln mit Pesto",
		ImgURL:      "https://img.chefkoch-cdn.de/rezepte/554411153143502/bilder/1393938/crop-600x400/spaghetti-mit-pesto.jpg",
		Ingredients: []string{"Nudeln", "Pesto"},
	}}
}
