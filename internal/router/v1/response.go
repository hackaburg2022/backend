package v1

type ResponseIngredient struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type ResponseAllergy struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type ResponseRecipe struct {
	ID          string   `json:"id"`
	Name        string   `json:"name"`
	ImgURL      string   `json:"imgUrl"`
	Ingredients []string `json:"ingredients"`
}
