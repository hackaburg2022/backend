package v1

import (
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
)

type reqRecipe struct {
	Likes    string `query:"likes"`
	Disliked string `query:"disliked"`
	Skip     int    `query:"skip"`
	Size     int    `query:"size"`
}

func (r *Router) GetRecipe(c echo.Context) error {
	req := reqRecipe{}

	if err := echo.QueryParamsBinder(c).
		String("likes", &req.Likes).
		String("disliked", &req.Disliked).
		Int("skip", &req.Skip).
		Int("size", &req.Size).BindError(); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	like := []string{}
	notLike := []string{}

	if req.Likes != "" {
		like = strings.Split(req.Likes, ",")
	}
	if req.Disliked != "" {
		notLike = strings.Split(req.Disliked, ",")
	}

	recipes, err := r.Repository.GetRecipesByLikedAndNotLikedIngredients(like, notLike, req.Size, req.Skip)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	return c.JSON(http.StatusOK, recipes)
}
