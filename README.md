# KochChef Backend

## Description

This project manages the backend for the kochchef

## Getting started

These instructions will guide you to get an copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

To use this project you need `golang` installed and also an running `elasticsearch` instance.

### Installing

To install this project you must switch into this directory and execute  
```bash
make build
```

### Configuration

To configure this project you must switch into the `config` directory. There is one `default.yaml` in which all available parameters are displayed. To modify it create a new `.yaml` in which you write all the parameters which are new.  

## Deployment

To start the project you can specify an `ENV` environment variable which loads the correct `.yaml` from the config folder. To start then the project you must execute
```bash
make start
```

#### Attention
The provided `.yaml` files in the `config` folder change the handeling of the Default variable value. To be sure delete all files except for the `default.yaml`

## Built With

* [Go](https://go.dev/) - The programming language
* [Elasticsearch](https://www.elastic.co/de/elasticsearch/) - Index Database

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/hackaburg2022/backend/tags). 

## Authors

* **Tobias Schneider** - *Initial work* - [cynt4k](https://gitlab.com/cynt4k)

See also the list of [contributors](https://gitlab.com/hackaburg2022/backend/develop) who participated in this project.

## License

This project is licensed under the commercial license - see the [LICENSE](LICENSE) file for details