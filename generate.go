//go:build ignore
// +build ignore

// The following directive is necessary to make the package coherent:
// This program generates contributors.go. It can be invoked by running
// go generate
package main

import (
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

type wireBuild struct {
	Name string
	Path string
}

func main() {
	buildTasks := make([]wireBuild, 0)
	currentDir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	buildTasks = append(buildTasks, wireBuild{
		Name: "cli",
		Path: "cmd",
	})

	buildTasks = append(buildTasks, wireBuild{
		Name: "router",
		Path: "internal/router",
	})

	if _, err := exec.LookPath("wire"); err != nil {
		log.Fatal("wire not installed - install it with 'go install github.com/google/wire/cmd/wire@latest'")
	}

	for _, entry := range buildTasks {
		cmd := exec.Command("wire", "gen")
		cmd.Dir = filepath.Join(currentDir, entry.Path)
		if _, err := cmd.Output(); err != nil {
			if exiterr, ok := err.(*exec.ExitError); ok {
				log.Fatalf("failed to gen %s: %s - ExitCode: %d", entry.Name, exiterr.Stderr, exiterr.ExitCode())
			}
			if pathErr, ok := err.(*os.PathError); ok {
				log.Fatalf("failed to gen %s: %s", entry.Name, pathErr.Err)
			}
			log.Fatalf("failed to gen %s %w", entry.Name, err)
		}
	}

	log.Println("successfully generated wire files")

	// cmd := exec.Command("wire", "gen")
	// cmd.Dir = filepath.Join(currentDir, "cmd")
	// _, err = cmd.Output()
	// if err != nil {
	// 	if exiterr, ok := err.(*exec.ExitError); ok {
	// 		log.Fatalf("failed to gen wire cmd %s - ExitCode: %d", exiterr.Stderr, exiterr.ExitCode())
	// 	}
	// }
}
