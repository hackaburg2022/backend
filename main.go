//go:generate go run generate.go
package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/hackaburg2022/backend/cmd"
)

const (
	Version = "unknown"
	Build   = "unknown"
)

func main() {
	// fmt.Println("hello world")
	fmt.Printf("starting kochchef backend version: %s - build %s\n", Version, Build)

	if err := cmd.Execute(); err != nil {
		log.Fatal(err)
	}

	ctx, shutdown := context.WithCancel(context.Background())
	defer shutdown()

	if err := cmd.Serve(); err != nil {
		log.Fatal(err)
	}

	err := waitForInterrupt(ctx)
	if err != nil {
		log.Fatal(err)
	}
}

func waitForInterrupt(ctx context.Context) error {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)

	select {
	case sig := <-c:
		return fmt.Errorf("received signal %s", sig)
	case <-ctx.Done():
		return errors.New("canceled")
	}
}
