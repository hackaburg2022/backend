package config

var c = &Config{}

type Elasticsearch struct {
	Host     string `yaml:"host"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
}

type Server struct {
	Host string `yaml:"host"`
	Port uint   `yaml:"port"`
}

type Config struct {
	Server        *Server        `yaml:"server"`
	Elasticsearch *Elasticsearch `yaml:"elasticsearch"`
	DevMode       bool
}

func GetConfig() *Config {
	return c
}
