//go:build wireinject
// +build wireinject

package cmd

import (
	"github.com/elastic/go-elasticsearch/v8"
	"github.com/google/wire"
	"gitlab.com/hackaburg2022/backend/cmd/config"
	"gitlab.com/hackaburg2022/backend/internal/repository"
	"gitlab.com/hackaburg2022/backend/internal/router"
	"go.uber.org/zap"
)

func newServer(config *config.Config, logger *zap.Logger, db *elasticsearch.Client) (*Server, error) {
	wire.Build(
		router.New,
		repository.NewRepository,
		wire.Struct(new(Server), "*"),
	)
	return nil, nil
}
