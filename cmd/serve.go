package cmd

import (
	"fmt"

	"github.com/labstack/echo/v4"
	"gitlab.com/hackaburg2022/backend/internal/repository"
	"go.uber.org/zap"
)

type Server struct {
	logger *zap.Logger
	Repo   repository.Repository
	echo   *echo.Echo
}

func Serve() error {
	logger := getLogger()

	db, err := getDatabase()
	if err != nil {
		return err
	}

	server, err := newServer(cfg, logger, db)
	if err != nil {
		return err
	}

	go func() {
		serverURL := fmt.Sprintf("%s:%d", cfg.Server.Host, cfg.Server.Port)
		logger.Sugar().Infof("serving api under: http://%s", serverURL)
		if err := server.Start(serverURL); err != nil {
			logger.Sugar().Fatal("error while serving server: %w", err)
		}
	}()

	return nil
}

func (s *Server) Start(address string) error {
	return s.echo.Start(address)
}
